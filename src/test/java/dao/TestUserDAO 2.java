/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import models.User;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author vamsi
 */
public class TestUserDAO {
    
    @Test
    public void testAll(){
        assertEquals(true, UserDAO.all().size()>0);
    }
    
    
    @Test
    public void get(){
        assertEquals("User 1", UserDAO.get(1).getName());
    }
    
    
    @Test
    public void add(){
        User user = new User();
        user.setName("Richard");
        user.setAddress("UK, London");
        user.setPhone("+0222555");
        user.setAdmin(false);
        user.setPhysician(true);
        assertEquals(true, UserDAO.add(user));
    }
}
