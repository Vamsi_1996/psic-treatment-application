/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import models.Appointment;
import models.User;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author alluriraviteja
 */
public class TestAppointment {
    
    @Test
    public void testAll(){
        assertEquals(true, AppointmentDAO.all().size()>0);
    }
    
    
    
    
    
    @Test
    public void add(){
        Appointment app = new Appointment();
        app.setPatienName("Richard");
        app.setPatientId(1);
        app.setPlanId(1);
        app.setStatus("Booked");
        
        
        assertEquals(true, AppointmentDAO.add(app));
    }
}
