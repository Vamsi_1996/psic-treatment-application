/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author vamsi
 */
public class TestUser {
    
    @Test
    public void testGetName(){
        User user = new User();
        user.setName("John Doe");
        user.setAddress("home");
        user.setAdmin(true);
        user.setPhone("01120023");
        user.setPhysician(false);
        assertEquals("John Doe", user.getName());
    }
}
