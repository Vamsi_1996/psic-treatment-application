/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Expertise;

/**
 *
 * @author vamsi
 */
public class ExpertiseDAO {
    private static final Connection CON = Util.getConnection();

    public static List<Expertise> all() {
        List<Expertise> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM expertise");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Expertise expertise = new Expertise();
                expertise.setId(rs.getInt("id"));
                expertise.setName(rs.getString("name"));
                list.add(expertise);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(Expertise data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO expertise (name) VALUES (?)");
            ps.setString(1, data.getName());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
