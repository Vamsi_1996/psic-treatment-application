/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vamsi
 */
public class Util {
    
    public static Connection getConnection(){   
        Connection conn=null;     
        try {
            String url = "jdbc:sqlite:database";
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);
        } catch (ClassNotFoundException | SQLException e) {
            Util.printError(e);
        }
        return conn;
    }
    

    public static void printError(Exception ex) {
        try {
            Logger LOG = Logger.getLogger(Util.class.getName());
            LOG.log(Level.SEVERE, null, ex);
        } catch (Exception ex1) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex1);
        }
    }
}
