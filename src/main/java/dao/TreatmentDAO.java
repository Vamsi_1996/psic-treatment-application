/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.*;

/**
 *
 * @author vamsi
 */
public class TreatmentDAO {

    private static final Connection CON = Util.getConnection();

    public static Treatment get(int id) {
        Treatment treatment = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM treatment WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                treatment = new Treatment();
                treatment.setId(rs.getInt("id"));
                treatment.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return treatment;
    }

    public static List<Treatment> all() {
        List<Treatment> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM treatment");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Treatment treatment = new Treatment();
                treatment.setId(rs.getInt("id"));
                treatment.setName(rs.getString("name"));
                list.add(treatment);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(Treatment data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO treatment (name) VALUES (?)");
            ps.setString(1, data.getName());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
