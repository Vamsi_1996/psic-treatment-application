/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.*;

/**
 *
 * @author vamsi
 */
public class AppointmentDAO {

    private static final Connection CON = Util.getConnection();

    public static Appointment get(int id) {
        Appointment appointment = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM appointment WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                appointment = new Appointment();
                appointment.setId(rs.getInt("id"));
                appointment.setPatientId(rs.getInt("patientId"));
                appointment.setPatienName(rs.getString("patientName"));
                appointment.setPlanId(rs.getInt("planId"));
                appointment.setStatus(rs.getString("status"));
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return appointment;
    }

    public static List<Appointment> all() {
        List<Appointment> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM appointment");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Appointment appointment = new Appointment();
                appointment.setId(rs.getInt("id"));
                appointment.setPatientId(rs.getInt("patientId"));
                appointment.setPatienName(rs.getString("patientName"));
                appointment.setPlanId(rs.getInt("planId"));
                appointment.setStatus(rs.getString("status"));
                list.add(appointment);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(Appointment data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO appointment (patientId, patientName, planId, status) VALUES (?,?,?,?)");
            ps.setInt(1, data.getPatientId());
            ps.setString(2, data.getPatienName());
            ps.setInt(3, data.getPlanId());
            ps.setString(4, data.getStatus());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }

    public static boolean update(Appointment data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("UPDATE appointment SET patientId=?, patientName=?, planId=?, status=? WHERE id=?");
            ps.setInt(1, data.getPatientId());
            ps.setString(2, data.getPatienName());
            ps.setInt(3, data.getPlanId());
            ps.setString(4, data.getStatus());
            ps.setInt(5, data.getId());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
