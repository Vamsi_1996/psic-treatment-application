/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.User;

/**
 *
 * @author vamsi
 */
public class UserDAO {

    private static final Connection CON = Util.getConnection();

    public static User get(int id) {
        User user = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM user WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setAdmin(rs.getBoolean("isAdmin"));
                user.setPhysician(rs.getBoolean("isPhysician"));
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return user;
    }

    public static List<User> all() {
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM user");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setAdmin(rs.getBoolean("isAdmin"));
                user.setPhysician(rs.getBoolean("isPhysician"));
                list.add(user);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(User data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO user (name, address, phone, isAdmin, isPhysician) VALUES (?,?,?,?,?)");
            ps.setString(1, data.getName());
            ps.setString(2, data.getAddress());
            ps.setString(3, data.getPhone());
            ps.setBoolean(4, data.isAdmin());
            ps.setBoolean(5, data.isPhysician());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
