/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Plan;

/**
 *
 * @author vamsi
 */
public class PlanDAO {

    private static final Connection CON = Util.getConnection();

    public static Plan get(int id) {
        Plan plan = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM plan WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                plan = new Plan();
                plan.setId(rs.getInt("id"));
                plan.setPhysicianId(rs.getInt("physicianId"));
                plan.setRoomId(rs.getInt("roomId"));
                plan.setTreatmentId(rs.getInt("treatmentId"));
                plan.setDate(rs.getString("date"));
                plan.setTime(rs.getString("time"));
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return plan;
    }

    public static List<Plan> all() {
        List<Plan> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM plan");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Plan plan = new Plan();
                plan.setId(rs.getInt("id"));
                plan.setPhysicianId(rs.getInt("physicianId"));
                plan.setRoomId(rs.getInt("roomId"));
                plan.setTreatmentId(rs.getInt("treatmentId"));
                plan.setDate(rs.getString("date"));
                plan.setTime(rs.getString("time"));
                list.add(plan);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(Plan data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO plan (physicianId, roomId, treatmentId, date, time) VALUES (?,?,?,?,?)");
            ps.setInt(1, data.getPhysicianId());
            ps.setInt(2, data.getRoomId());
            ps.setInt(3, data.getTreatmentId());
            ps.setString(4, data.getDate());
            ps.setString(5, data.getTime());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
    
    
    public static boolean update(Plan data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("UPDATE plan SET physicianId=?, roomId=?, treatmentId=?, date=?, time=? WHERE id=?");
            ps.setInt(1, data.getPhysicianId());
            ps.setInt(2, data.getRoomId());
            ps.setInt(3, data.getTreatmentId());
            ps.setString(4, data.getDate());
            ps.setString(5, data.getTime());
            ps.setInt(6, data.getId());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
