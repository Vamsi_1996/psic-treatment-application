/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Room;

/**
 *
 * @author vamsi
 */
public class RoomDAO {
    private static final Connection CON = Util.getConnection();

    public static Room get(int id) {
        Room room = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM room WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                room = new Room();
                room.setId(rs.getInt("id"));
                room.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return room;
    }

    public static List<Room> all() {
        List<Room> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM room");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Room room = new Room();
                room.setId(rs.getInt("id"));
                room.setName(rs.getString("name"));
                list.add(room);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(Room data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO room (name) VALUES (?)");
            ps.setString(1, data.getName());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
