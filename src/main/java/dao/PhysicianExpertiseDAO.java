/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.*;

/**
 *
 * @author vamsi
 */
public class PhysicianExpertiseDAO {
    
    private static final Connection CON = Util.getConnection();

    public static List<PhysicianExpertise> all(int physicianId) {
        List<PhysicianExpertise> list = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM physicianexpertise where physicianId=?");
            ps.setInt(1, physicianId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PhysicianExpertise physicianexpertise = new PhysicianExpertise();
                physicianexpertise.setId(rs.getInt("id"));
                physicianexpertise.setPhysicianId(rs.getInt("physicianId"));
                physicianexpertise.setExpertise(rs.getString("expertise"));
                list.add(physicianexpertise);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return list;
    }

    public static boolean add(PhysicianExpertise data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO physicianexpertise (physicianId, expertise) VALUES (?,?)");
            ps.setInt(1, data.getPhysicianId());
            ps.setString(2, data.getExpertise());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;
    }
}
